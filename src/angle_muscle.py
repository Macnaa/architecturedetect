
import math
import random
import statistics as st
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.cluster import DBSCAN

from length_muscle import apply_brightness_contrast

def centroid(ls):
    """ Returns centroid for list of tuples. """
    return (st.mean(i[0] for i in ls), st.mean(i[1] for i in ls))

def contour_to_tuples(contour):
    """ Transforms a contour into a list of tuples """
    return [(i[0][0], i[0][1]) for i in contour]

def euclidean_distance(a, b):
    """ Efficient calculation of euclidean distance """
    a_n = np.array(a)
    b_n = np.array(b)
    return np.linalg.norm(a_n - b_n)

def muscle_angle_detection(us_name, u_bounds, l_bounds, verbose=False, fig=None):

    """
        Attempts to detect the angle of striations in the muscles fibers
        in this specific image format. Treats upper and lower muscle
        fiber differently.

        Arguments:
        us_name -- Image path.
        u_bounds -- y-bounds of the upper half of the image.
        l_bounds -- y-bounds of the lower half of the image.
        verbose -- Prints angles on images.

        returns -> angle of striation in upper and lower image.
    """

    img = cv.imread(us_name, 0)
    crop_img_u = img[u_bounds[0]:u_bounds[1], :]
    crop_img_l = img[l_bounds[0]:l_bounds[1], :]

    u_angle = find_angle_striation(crop_img_u, expected=-1, verbose=verbose, fig=fig)
    l_angle = find_angle_striation(crop_img_l, expected=1, verbose=verbose, fig=fig)

    return u_angle, l_angle

def aggregate_grads(gs, expected):
    """ Returns an aggrigation of gradients into one gradient. """
    if expected == 1:
        return np.percentile(list(filter(lambda x: x < 0, gs)), 25)
        # return st.median(filter(lambda x: x < 0, gs))
    elif expected == -1:
        return st.median(filter(lambda x: x > 0, gs))
    else:
        gradient = st.mean(gs)
        gs = filter(lambda g: g > 0, gs) \
            if gradient > 0 else filter(lambda g: g < 0, gs)
        return st.median(gs)

def contours_to_gradient_l(contours, expected):

    gradient_list = list()
    for e in contours:
        edge_tuples = contour_to_tuples(e)
        X = np.array([i[0] for i in edge_tuples]).reshape(-1, 1)
        y = [(i[1]) for i in edge_tuples]

        c = centroid(edge_tuples)

        reg = LinearRegression().fit(X, y)
        gradient_list.append((c, (c[0] + 30, c[1] + 30*reg.coef_[0]), reg.coef_[0]))

    return gradient_list

def contours_to_gradient_l_centroids(contours, expected):

    centroids = list()
    for contour in contours:
        e = contour_to_tuples(contour)
        centroids.append(centroid(e))

    gradient_list = list()
    for c in centroids:

        if centroids:
            sorted_c_list = sorted(centroids, key=lambda c1: euclidean_distance(c, c1))[1:]

            for c1 in sorted_c_list:
                if c1[1] == c[1]:
                    continue
                else:
                    gradient = (c1[0] - c[0]) / (c1[1] - c[1])

                # if (gradient)*expected > 0:
                gradient_list.append((c, c1, euclidean_distance(c, c1), gradient))
                break
                    # break

    return gradient_list

def contours_to_gradient_l_clustering(img, contours, expected):

    coordinates = list()
    for contour in contours:
        e = contour_to_tuples(contour)
        coordinates.extend(map(list, e))
    X = np.array(coordinates)

    dbscan = DBSCAN(eps=10, min_samples=10).fit(X)
    labels = dbscan.labels_

    clusters = dict()
    for idx in range(len(coordinates)):
        try:
            clusters[labels[idx]].append(coordinates[idx])
        except KeyError:
            clusters[labels[idx]] = [coordinates[idx]]

    gradient_list = list()
    for c in clusters.keys():
        if c == -1:
            continue

        X = np.array([i[0] for i in clusters[c]]).reshape(-1, 1)
        y = [i[1] for i in clusters[c]]

        reg = LinearRegression().fit(X, y)

        gradient = reg.coef_[0]
        c = centroid(clusters[c])
        c_direct = (c[0] + 30, c[1] + 30*gradient)

        gradient_list.append((c, c_direct, gradient))

    # plt.plot(), plt.imshow(img, cmap = 'gray')
    # plt.title('Original Image'), plt.xticks([]), plt.yticks([])

    # for c in clusters.keys():
        # ran = (random.uniform(0, 1), random.uniform(0, 1), random.uniform(0, 1))
        # x = [j[0] for j in clusters[c]]
        # y = [j[1] for j in clusters[c]]
        # plt.scatter(x, y, c=ran)

    # plt.show()
    # input()

    return gradient_list

def find_angle_striation(img, expected=None, verbose=False, fig=None):
    """ Find the angle of striations in an image. """

    img = apply_brightness_contrast(img, 0, 100)
    threshold_image = img #cv.Canny(img, 80, 150)
    _, contours, _ = cv.findContours(threshold_image, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)

    if expected == 1:
        centroid_list = contours_to_gradient_l_clustering(img, contours, expected)
    else:
        centroid_list = contours_to_gradient_l(contours, expected)

    gradient_list = [i[2] for i in centroid_list]
    partner_list = [(i[0], i[1]) for i in centroid_list]

    gradient = aggregate_grads(gradient_list, expected)
    angle = math.atan(gradient)

    if verbose:
        # plt.subplot(121), plt.imshow(img, cmap = 'gray')
        # plt.title('Original Image'), plt.xticks([]), plt.yticks([])

        s = 0 if expected < 1 else 1

        fig[1][1][s].imshow(img, cmap = 'gray')
        for i in partner_list:
            x = [j[0] for j in i]
            y = [j[1] for j in i]
            fig[1][1][s].plot(x, y)

        if gradient > 0:
            fig[1][1][s].plot([0, img.shape[1]], [0, gradient*img.shape[1] ], linewidth=7, color='yellow')
        else:
            fig[1][1][s].plot([0, img.shape[1]], [img.shape[0], img.shape[0] + gradient*img.shape[1]], linewidth=7, color='yellow')

        # plt.subplot(121)

        # for i in partner_list:
            # x = [j[0] for j in i]
            # y = [j[1] for j in i]
            # plt.plot(x, y)

        # if gradient > 0:
            # plt.plot([0, img.shape[1]], [0, gradient*img.shape[1] ], linewidth=7, color='yellow')
        # else:
            # plt.plot([0, img.shape[1]], [img.shape[0], img.shape[0] + gradient*img.shape[1]], linewidth=7, color='yellow')

        # plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
        # plt.subplot(122), plt.imshow(threshold_image, cmap = 'gray')

        # plt.show()
        # input()

    return math.degrees(angle)

