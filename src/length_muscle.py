
import math
import statistics as st
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.cluster import KMeans
import utility as ut

flatten = lambda l: [item for sublist in l for item in sublist]

def PolyArea(xy):
    """ Returns the area of a polygon discribed by a set
        of two dimensional vertices"""
    x = [i[0] for i in xy]
    y = [i[1] for i in xy]
    return 0.5*np.abs(np.dot(x, np.roll(y, 1))-np.dot(y, np.roll(x, 1)))

def get_distances(ob1, ob2):
    """ Returns the mean distance between corrosponding elements
        of a list, where the elements are (x, y) """
    return st.mean(math.sqrt(
        (ob1[i][0] - ob2[i][0])**2 + (ob1[i][1] - ob2[i][1])**2) for i in range(len(ob1)))

def inside_angle(top, bottom):

    t1, t2 = top[0], top[-1]
    b1, b2 = bottom[0], bottom[-1]

    v1 = np.array([ t2[0] - t1[0], t2[1] - t1[1] ])
    v2 = np.array([ b2[0] - b1[0], b2[1] - b1[1] ])

    dot = np.dot(v1, v2)
    v1_mag = np.linalg.norm(v1)
    v2_mag = np.linalg.norm(v2)
    mag_form = dot/ (v1_mag * v2_mag)

    # print("top: %s, bottome: %s" %(top, bottom))
    # print("t1,t2 = %s, %s" % (t1,t2))
    # print("b1,b2 = %s, %s" % (b1,b2))
    # print("v1 = %s" % v1)
    # print("v2 = %s" % v2)
    # print("v1_mag, v2_mag = %s, %s " % (v1_mag, v2_mag))
    # print("dot, mag = %s, %s " % (dot , mag_form))

    rads = math.acos(mag_form)

    return math.degrees(rads)

def cluster_edges(tuples, img):
    flat = ut.flatten(tuples)
    X = np.array(flat)
    kmeans = KMeans(n_clusters=3, random_state=0).fit(X)

    cluster_tuples = list()
    for f in tuples:

        sum_cluster_f = 0.0

        for e in f:
            idx = flat.index(e)

            sum_cluster_f += kmeans.labels_[idx]

        cluster_f = int(round(sum_cluster_f / len(f)))
        cluster_tuples.append(cluster_f)

    # plt.imshow(img)

    # colors = ['red', 'blue', 'yellow']
    # for e,l in zip(tuples, cluster_tuples):
        # x = [i[0] for i in e]
        # y = [i[1] for i in e]
        # plt.scatter(x, y, color=colors[l])
    # plt.show()
    # input()

    return zip(tuples, cluster_tuples)


def determine_edges(sorted_tuples, verbose, us_name, img):

    top_dist = get_distances(sorted_tuples[1], sorted_tuples[2])
    bottom_dist = get_distances(sorted_tuples[3], sorted_tuples[4])

    if top_dist == 0 or bottom_dist == 0:
        return True, sorted_tuples

    ratio = top_dist / bottom_dist

    if 0.25 < ratio < 1 or 0.25 < (1 / ratio) < 1:
        return False, sorted_tuples
    else:
        return True, sorted_tuples

def muscle_size_detection(us_name, verbose=False, fig=None):

    """
        Attempts to detect the width of the two muscles
        in this specific image format.

        Arguments:
        us_name -- Image path.
        verbose -- Prints angles on images.

        returns -> lengths of muscles in upper and lower image
            and bounded region between.
    """

    img = cv.imread(us_name, 0)
    blurred_image = cv.GaussianBlur(img, (23, 13), 30)
    contrast_image = apply_brightness_contrast(blurred_image, 0, 90)

    edge_tuples, raw_edges = construct_edge_features(contrast_image)

    if edge_tuples is None:
        return None

    fault, edge_tuples = determine_edges(sorted( edge_tuples, key=lambda l: st.mean(int(i[1]) for i in l) ), True, us_name, img)

    if fault:
        return None

    top_dist = get_distances(edge_tuples[1], edge_tuples[2])
    bottom_dist = get_distances(edge_tuples[3], edge_tuples[4])

    AA_top = inside_angle(edge_tuples[1], edge_tuples[2])
    AA_bottom = inside_angle(edge_tuples[3], edge_tuples[4])

    u_bounds = (int(max(i[1] for i in edge_tuples[1])), int(min(i[1] for i in edge_tuples[2])))
    l_bounds = (int(max(i[1] for i in edge_tuples[3])), int(min(i[1] for i in edge_tuples[4])))

    if verbose:
        print("------------------")

        fig[1][0][0].imshow(contrast_image, cmap = 'gray')
        colors = ['red', 'yellow', 'blue']
        for e in raw_edges:
            x = [i[0] for i in e[0]]
            y = [i[1] for i in e[0]]
            fig[1][0][0].scatter(x, y, color=colors[e[1]])
            fig[1][0][0].text(x[0], y[0], str(raw_edges.index(e)))

        fig[1][0][1].imshow(img, cmap = 'gray')
        for e in edge_tuples:
            x = [i[0] for i in e]
            y = [i[1] for i in e]
            fig[1][0][1].scatter(x, y)
            fig[1][0][1].text(x[0], y[0], str(edge_tuples.index(e)))

        print("Top Distance: %s" % top_dist)
        print("Bottom Distance: %s" % bottom_dist)
        print("------------------")



        # plt.subplot(121), plt.imshow(img, cmap = 'gray')
        # plt.title('Original Image'), plt.xticks([]), plt.yticks([])

        # plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
        # plt.subplot(122), plt.imshow(img, cmap = 'gray')

        # plt.subplot(122)
        # for e in edge_tuples:
            # x = [i[0] for i in e]
            # y = [i[1] for i in e]
            # plt.scatter(x, y)
            # plt.text(x[0], y[0], str(edge_tuples.index(e)))

        # print("Top Distance: %s" % top_dist)
        # print("Bottom Distance: %s" % bottom_dist)

        # plt.show()
        # input()

    return top_dist, bottom_dist, AA_top, AA_bottom, u_bounds, l_bounds

def apply_brightness_contrast(input_img, brightness = 0, contrast = 0):
    """ Returns an image which is a copy of input_img which has been
        brightened by <brightness> and constrasted by <contrast>."""

    if brightness != 0:
        if brightness > 0:
            shadow = brightness
            highlight = 255
        else:
            shadow = 0
            highlight = 255 + brightness
        alpha_b = (highlight - shadow)/255
        gamma_b = shadow

        buf = cv.addWeighted(input_img, alpha_b, input_img, 0, gamma_b)
    else:
        buf = input_img.copy()

    if contrast != 0:
        f = 131*(contrast + 127)/(127*(131-contrast))
        alpha_c = f
        gamma_c = 127*(1-f)

        buf = cv.addWeighted(buf, alpha_c, buf, 0, gamma_c)

    return buf

def pick_largest_edges(sort, n):
    cluster = []
    count = 0
    chosen = []
    for s in sort:
        if s[1] not in cluster:
            chosen.append(s[0])
            cluster.append(s[1])
            count += 1
        if count >= n:
            return chosen
    return chosen

def construct_edge_features(threshold_image):
    """ Returns a list of muscles edges from an image. """

    feature_list = list()
    _, connected_edges, hierarchy = cv.findContours(threshold_image, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)

    connected = list()

    for e in connected_edges:
        edge_tuples = [(i[0][0], i[0][1]) for i in e]
        connected.append(edge_tuples)

    def PolyArea_label(xyl):
        return PolyArea(xyl[0])


    labelled_edges = list(cluster_edges(connected, threshold_image))
    sorted_edges = sorted(labelled_edges, key=PolyArea_label, reverse=True)

    final_edges = pick_largest_edges(sorted_edges, 3)
    if len(final_edges) < 3:
        return None, None

    connected = list()
    (y_max, x_max) = threshold_image.shape

    for e in final_edges:
        edge_tuples = e
        X = np.array([i[0] for i in edge_tuples]).reshape(-1, 1)
        y = [(i[1]) for i in edge_tuples]

        reg = LinearRegression().fit(X, y)
        stdev = np.float64(st.stdev(map(int, y)))*0.75

        edge_top = list(zip(np.linspace(0, x_max, 30),
                np.linspace(reg.intercept_ - stdev, reg.intercept_ + reg.coef_*x_max - stdev, 30)))
        edge_bottom = list(zip(np.linspace(0, x_max, 30),
            np.linspace(reg.intercept_ + stdev, reg.intercept_ + reg.coef_*x_max + stdev, 30)))

        connected.append(edge_top)
        connected.append(edge_bottom)


    return connected, labelled_edges
