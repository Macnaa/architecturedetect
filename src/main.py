
import os
import sys
import tempfile
import random
import string
import math
import argparse

import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt

import length_muscle as lm
import angle_muscle as am
from utility import flatten, expand_mask

def remove_color(img, color):

    (lower, upper) = color
    lower = np.array(lower, dtype="uint8")
    upper = np.array(upper, dtype="uint8")

    mask = cv.inRange(img, lower, upper)
    expanded_mask = expand_mask(mask, 255, 5, inplace=True)

    img = cv.inpaint(img, mask, 3, cv.INPAINT_TELEA)
    return img

def remove_edge_bars(img, error):

    (y_max, x_max, _) = img.shape

    cutoff = None
    for i in range(0, x_max):
        pixel = img[round(y_max / 2), i]
        all_zero = all(v <= error for v in pixel)
        # print("%s, %s, %s" % (i, str(pixel), all_zero))
        if not all_zero:
            cutoff = i
            break

    if cutoff > 0:
        img = img[:, cutoff:-cutoff]

    return img

def preprocess_image(file_name):

    temp_dir = tempfile.gettempdir() + "/"
    tf_ext = ".jpg"
    while True:
        tfname = ''.join(random.choice(string.ascii_letters) for i in range(20))
        tf_path = temp_dir + tfname + tf_ext
        if not os.path.isfile(tf_path):
            break

    yellow = ([160, 160, 0], [255, 255, 150])
    img = cv.imread(file_name)
    img = cv.cvtColor(img, cv.COLOR_BGR2RGB)
    img = remove_color(img, yellow)
    img = remove_edge_bars(img, 8)

    cv.imwrite(tf_path, img)

    return tf_path

def argument_parser():
    """
    Function: wraps argument parser
    Return Parser
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", action='store_true',
                        help="Verbose output")
    parser.add_argument("-o", "--output-name", type=str,
                        help="Specify output file")
    parser.add_argument("-i", "--input-name", type=str,
                        help="Specify input folder")
    return parser

def main():
    mypath = "../input"
    output_file_name = "../output/output.csv"
    verbose = False

    parser = argument_parser()
    args = parser.parse_args()

    if args.output_name is not None:
        output_file_name = args.output_name
    if args.verbose is not None:
        verbose = args.verbose
    if args.input_name is not None:
        mypath = args.input_name

    with open(output_file_name,  "w") as out:

        out.write("image, top_length, bottom_length, top_angle, bottom_angle, AA_top, AA_bottom, fl_top, fl_bottom\n")
        for f in os.listdir(mypath):
            print("Processing: %s" % f)
            file_name = mypath + "/" + f
            new_file_name = preprocess_image(file_name)

            # try:
            if verbose:
                fig_ob = plt.subplots(ncols=2, nrows=2)
                success  = lm.muscle_size_detection(new_file_name, verbose=verbose, fig=fig_ob)
            else:
                success  = lm.muscle_size_detection(new_file_name)

            if success is None:
                print("Error: cannot process %s" % file_name)
                continue
            td, bd, AA_top, AA_bottom, u_bounds, l_bounds = success

            if verbose:
                ta, ba = am.muscle_angle_detection(new_file_name, u_bounds, l_bounds, verbose=verbose, fig=fig_ob)
            else:
                ta, ba = am.muscle_angle_detection(new_file_name, u_bounds, l_bounds)

            fig_ob[0].show()




            fl_top = (math.sin(math.radians(AA_top) + math.pi/2)*td ) / math.sin(math.pi - (math.radians(AA_top) + math.pi - ta))
            fl_bottom = (math.sin(math.radians(AA_bottom) + math.pi/2)*bd ) / math.sin(math.pi - (math.radians(AA_bottom) + math.pi - ba))

            out.write("%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % (f, td, bd, ta, ba, AA_top, AA_bottom, fl_top, fl_bottom))
            # except Exception as e:
                # print("Error: cannot process %s because %s" % (file_name, e))
                # continue


        input()
        exit(0)

if __name__ == "__main__":
    main()

