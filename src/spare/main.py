
from random import random
import os
import sys
import math
import statistics as st
import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans
from sklearn.linear_model import LinearRegression
from sklearn.mixture import GaussianMixture
import sklearn.ensemble
import scipy

flatten = lambda l: [item for sublist in l for item in sublist]

def PolyArea(xy):
    x = [i[0] for i in xy]
    y = [i[1] for i in xy]
    return 0.5*np.abs(np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))

def min_dist(point, ob):
    x1, y1 = point
    return min(math.sqrt((x1 - x2)**2 + (y1 - y2)**2) for x2,y2 in ob)

def get_distances(ob1,ob2):
    return st.mean(math.sqrt(
        (ob1[i][0] - ob2[i][0])**2 + (ob1[i][1] - ob2[i][1])**2) for i in range(len(ob1)))
    # return st.mean(min_dist(i,ob2) for i in ob1)

def muscle_size_detection(us_name):

    print("------------------")
    print(us_name)

    img = cv.imread(us_name,0)
    blurred_image = cv.GaussianBlur(img,(23, 13), 30)
    contrast_image = apply_brightness_contrast(blurred_image, 0,100)

    edge_features = construct_edge_features(contrast_image)
    edge_tuples = sorted(edge_tuples,key=lambda l: st.mean(int(i[1]) for i in l))

    top_dist = get_distances(edge_tuples[1], edge_tuples[2])
    bottom_dist = get_distances(edge_tuples[3], edge_tuples[4])

    plt.subplot(121),plt.imshow(img,cmap = 'gray')
    plt.title('Original Image'), plt.xticks([]), plt.yticks([])

    plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
    plt.subplot(122),plt.imshow(contrast_image,cmap = 'gray')

    plt.subplot(122)
    for e in edge_tuples:
        x = [i[0] for i in e]
        y = [i[1] for i in e]
        plt.scatter(x,y)
        plt.text(x[0],y[0],str(edge_tuples.index(e)))

    print("Top Distance: %s" % top_dist)
    print("Bottom Distance: %s" % bottom_dist)

    plt.show()
    input()

def apply_brightness_contrast(input_img, brightness = 0, contrast = 0):

    if brightness != 0:
        if brightness > 0:
            shadow = brightness
            highlight = 255
        else:
            shadow = 0
            highlight = 255 + brightness
        alpha_b = (highlight - shadow)/255
        gamma_b = shadow

        buf = cv.addWeighted(input_img, alpha_b, input_img, 0, gamma_b)
    else:
        buf = input_img.copy()

    if contrast != 0:
        f = 131*(contrast + 127)/(127*(131-contrast))
        alpha_c = f
        gamma_c = 127*(1-f)

        buf = cv.addWeighted(buf, alpha_c, buf, 0, gamma_c)

    return buf


def cluster_edges(edge_tuples, features):
    # kmeans = KMeans(n_clusters=6, random_state=0).fit(features)
    gm = GaussianMixture(n_components=6, random_state=0).fit_predict(features)

    clusters = dict()
    for i in range(0, len(edge_tuples)):
        try:
            clusters[gm[i]].append(edge_tuples[i])
        except KeyError:
            clusters[gm[i]] = [edge_tuples[i]]

    f = flatten(clusters.values())

    # plt.subplot(122)
    # for e in edge_tuples:
        # x = [i[0] for i in e]
        # y = [-i[1] for i in e]
        # plt.scatter(x,y)

    return clusters

def remove_outliers(cluster):
    # centroids = list()
    # for e in cluster:
        # Cx = st.mean(x for x,y in e)
        # Cy = st.mean(y for x,y in e)
        # centroids.append([Cx,Cy])
    # isol_forest = sklearn.ensemble.IsolationForest(contamination='auto', behaviour='new').fit(centroids)
    # isol_forest_dec = isol_forest.decision_function(centroids)
    # new_cluster = list()
    # for i in range(0,len(cluster)):
        # if isol_forest_dec[i] > 0:
            # new_cluster.append(cluster[i])
    new_cluster = cluster.copy()

    cluster_flat = [[x,y] for x,y in flatten(cluster)]

    isol_forest = sklearn.ensemble.IsolationForest(contamination='auto', behaviour='new').fit(cluster_flat)
    isol_forest_dec = isol_forest.decision_function(cluster_flat)

    for i in range(0, len(cluster_flat)):
        if isol_forest_dec[i] < 0:
            for e in new_cluster:
                if tuple(cluster_flat[i]) in e:
                    e.remove(tuple(cluster_flat[i]))


    return new_cluster

def construct_edge_features(threshold_image):
    feature_list = list()
    connected_edges, hierarchy = cv.findContours(threshold_image,cv.RETR_TREE,cv.CHAIN_APPROX_NONE)

    # connected_features = list()
    connected = list()

    for e in connected_edges:
        edge_tuples = [(i[0][0],i[0][1]) for i in e]
        connected.append(edge_tuples)

    sorted_edges = sorted(connected, key=PolyArea,reverse=True)[0:3]
    final_edges = sorted_edges
    # final_edges = list()
    # old_s = sorted_edges[0]
    # for s in sorted_edges:
        # if len(s) / len(old_s) > (1/3):
            # final_edges.append(s)
        # else:
            # break

    connected = list()
    (y_max,x_max) = threshold_image.shape
    print(threshold_image.shape)

    for e in final_edges:
        edge_tuples = e
        X = np.array([i[0] for i in edge_tuples]).reshape(-1,1)
        y = [(i[1]) for i in edge_tuples]

        reg = LinearRegression().fit(X,y)
        stdev = np.float64(st.stdev(map(int,y)))*0.75

        edge_top = list(zip(np.linspace(0,x_max,30),
                np.linspace(reg.intercept_ - stdev, reg.intercept_ + reg.coef_*x_max - stdev,30)))
        edge_bottom = list(zip(np.linspace(0,x_max,30),
            np.linspace(reg.intercept_ + stdev, reg.intercept_ + reg.coef_*x_max + stdev, 30)))

        # c1 = (0, reg.intercept_ - stdev)
        # c2 = (0, reg.intercept_ + stdev)
        # c3 = (x_max, reg.intercept_ + reg.coef_*x_max + stdev)
        # c4 = (x_max, reg.intercept_ + reg.coef_*x_max - stdev)

        # edge_tuples = [c1,c2,c3,c4]

        # pos = True if reg.coef_[0] > 0 else False
        # features = [ (reg.coef_), reg.intercept_, pos]
        # features = [ min(y), max(y), F*(reg.coef_[0])]

        # connected_features.append(features)
        connected.append(edge_top)
        connected.append(edge_bottom)


    return connected#, connected_features




def main():
    mypath = "../input"
    output_file_name = "../output/output.dat"
    if sys.argv:
        output_file_name = sys.argv[0]

    with open(output_file_name, "w") as out:

        out.write("image,top_length,bottom_length,top_angle,bottom_angle")
        for f in os.listdir(mypath):
            file_name = mypath + "/" + f
            td,bd = muscle_size_detection(file_name)
            ta,ba = muscle_angle_detection(file_name)

            out.write("%s,%s,%s,%s,%s" % (f,td,bd,ta,ba))

if __name__ == "__main__":
    main()

