
import numpy as np

flatten = lambda l: [item for sublist in l for item in sublist]

def expand_mask(mask, expand_unit, radius, inplace=False):
    (ys, xs) = np.where(mask == expand_unit)

    if not inplace:
        new_mask = mask.copy()
        mask = new_mask

    for i in range(len(xs)):
        y = ys[i]
        x = xs[i]

        for i in range(radius):
            for j in range(radius):
                if i + j <= radius:
                    try:
                        mask[y + j, x + i] = expand_unit
                    except IndexError:
                        pass
                    try:
                        mask[y - j, x + i] = expand_unit
                    except IndexError:
                        pass
                    try:
                        mask[y + j, x - i] = expand_unit
                    except IndexError:
                        pass
                    try:
                        mask[y - j, x - i] = expand_unit
                    except IndexError:
                        pass
    return mask
